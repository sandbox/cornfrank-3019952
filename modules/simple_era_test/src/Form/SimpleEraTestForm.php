<?php

namespace Drupal\simple_era_test\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Defines a form to test the simple era form element.
 */
class SimpleEraTestForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $node = Node::load(2);

    $form['standard'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Standard autocomplete'),
      '#target_type' => 'node',
      '#selection_settings' => [
        'target_bundles' => ['page'],
        'match_operator' => 'CONTAINS',
      ],
      '#default_value' => $node,
      '#autocreate' => ['bundle' => 'page'],
    ];

    $form['simple_era'] = [
      '#type' => 'simple_era',
      '#title' => $this->t('Simple ERA autocomplete'),
      '#target_type' => 'node',
      '#selection_settings' => [
        'target_bundles' => ['page'],
        'match_operator' => 'CONTAINS',
      ],
      '#autocreate' => ['bundle' => 'page'],
      '#tags' => TRUE,
      '#default_value' => $node,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'simple_era_test_form';
  }

  /**
   * {@inheritDoc}
   */
//   public function validateForm(array &$form, FormStateInterface $form_state) {
//     $title = $form_state->getValue('title');
//     if (strlen($title) < 5) {
//       // Set an error for the form element with a key of "title".
//       $form_state->setErrorByName('title', $this->t('The title must be at least 5 characters long.'));
//     }
//   }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();
    dpm($form_state->getValues());
  }

}
