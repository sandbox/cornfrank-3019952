# Simple Entity Reference Autocomplete

Provides a simple alternative to the core Drupal entity reference autocomplete.

## Features
- Hides the entity id from the user.
- When not in tags (comma separated values) mode, does not wrap labels that contain commas with quote marks.
- Core functionality is still available by using the core form element and autocomplete widgets.

## Installation

Download the module to the site modules folder ane enable it.

## Usage

- Use the **simple_era** form element in place of the core **entity_autocomplete** form element.
- Use the **Simple ERA tags** and **Simple ERA** widgets place of the core widgets.