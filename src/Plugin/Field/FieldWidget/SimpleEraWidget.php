<?php
namespace Drupal\simple_era\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteTagsWidget;

/**
 * Plugin implementation of the 'simple_era' widget.
 *
 * @FieldWidget(
 *   id = "simple_era",
 *   label = @Translation("Simple ERA"),
 *   description = @Translation("Simple autocomplete text field."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class SimpleEraWidget extends EntityReferenceAutocompleteWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['target_id']['#type'] = 'simple_era';
    return $element;
  }

}
