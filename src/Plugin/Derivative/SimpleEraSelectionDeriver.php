<?php
namespace Drupal\simple_era\Plugin\Derivative;

use Drupal\Core\Entity\Plugin\Derivative\DefaultSelectionDeriver;

/**
 * Provides derivative plugins for the SimpleEraSelection plugin.
 */
class SimpleEraSelectionDeriver extends DefaultSelectionDeriver {

}