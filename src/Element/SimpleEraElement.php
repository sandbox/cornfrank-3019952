<?php

namespace Drupal\simple_era\Element;

use Drupal\Component\Utility\Tags;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the simple_era form element.
 *
 * Extends the default form element to remove entity ids and quotes.
 *
 * @FormElement("simple_era")
 */
class SimpleEraElement extends EntityAutocomplete {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#selection_handler'] = 'simple_era';

    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    // Process the #default_value property.
    if ($input === FALSE && isset($element['#default_value']) && $element['#process_default_value']) {
      if (is_array($element['#default_value']) && $element['#tags'] !== TRUE) {
        throw new \InvalidArgumentException('The #default_value property is an array but the form element does not allow multiple values.');
      }
      elseif (!empty($element['#default_value']) && !is_array($element['#default_value'])) {
        // Convert the default value into an array for easier processing in
        // static::getEntityLabels().
        $element['#default_value'] = [$element['#default_value']];
      }

      if ($element['#default_value']) {
        if (!(reset($element['#default_value']) instanceof EntityInterface)) {
          throw new \InvalidArgumentException('The #default_value property has to be an entity object or an array of entity objects.');
        }

        // Extract the labels from the passed-in entity objects, taking access
        // checks into account.
        return static::getEntityLabels($element['#default_value'], $element['#tags']);
      }
    }

    // Potentially the #value is set directly, so it contains the 'target_id'
    // array structure instead of a string.
    if ($input !== FALSE && is_array($input)) {
      $entity_ids = array_map(function (array $item) {
        return $item['target_id'];
      }, $input);

        $entities = \Drupal::entityTypeManager()
          ->getStorage($element['#target_type'])
          ->loadMultiple($entity_ids);

        return static::getEntityLabels($entities, $element['#tags']);
    }
  }

  /**
   * {@inheritdoc}
   *
   * Overridden to add tags parameter.
   */
  public static function getEntityLabels(array $entities, $tags = TRUE) {
    /** @var \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository */
    $entity_repository = \Drupal::service('entity.repository');

    $entity_labels = [];
    foreach ($entities as $entity) {
      // Set the entity in the correct language for display.
      $entity = $entity_repository->getTranslationFromContext($entity);

      // Use the special view label, since some entities allow the label to be
      // viewed, even if the entity is not allowed to be viewed.
      $label = ($entity->access('view label'))
        ? $entity->label() : t('- Restricted access -');

      // Take into account "autocreated" entities.
//       if (!$entity->isNew()) {
//         $label .= ' (' . $entity->id() . ')';
//       }

      // Labels containing commas or quotes must be wrapped in quotes.
//       $entity_labels[] = Tags::encode($label);
      $entity_labels[] = $tags ? Tags::encode($label) : $label;
    }

    return implode(', ', $entity_labels);
  }

  /**
   * {@inheritdoc}
   */
  public static function processEntityAutocomplete(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $element['#selection_settings']['tags'] = $element['#tags'];
    $element = parent::processEntityAutocomplete($element, $form_state, $complete_form);

    if (!empty($element['#autocomplete_route_name'])) {
      $element['#autocomplete_route_name'] = 'simple_era.autocomplete';
    }
    return $element;
  }

}
