<?php

namespace Drupal\simple_era\Controller;

use Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityAutocompleteMatcher;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides route responses for the Simple Entity Reference Autocomplete module.
 */
class SimpleEraController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The autocomplete matcher for entity references.
   *
   * @var \Drupal\Core\Entity\EntityAutocompleteMatcher
   */
  protected $matcher;

  /**
   * The key value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValue;

  /**
   * Constructs a EntityAutocompleteController object.
   *
   * @param \Drupal\Core\Entity\EntityAutocompleteMatcher $matcher
   *   The autocomplete matcher for entity references.
   * @param \Drupal\Core\KeyValueStore\KeyValueStoreInterface $key_value
   *   The key value factory.
   */
  public function __construct(EntityAutocompleteMatcher $matcher, KeyValueStoreInterface $key_value) {
    $this->matcher = $matcher;
    $this->keyValue = $key_value;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('simple_era.autocomplete_matcher'),
        $container->get('keyvalue')->get('entity_autocomplete')
        );
  }

  /**
   * Autocomplete the label of an entity.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object that contains the typed tags.
   * @param string $target_type
   *   The ID of the target entity type.
   * @param string $selection_handler
   *   The plugin ID of the entity reference selection handler.
   * @param string $selection_settings_key
   *   The hashed key of the key/value entry that holds the selection handler
   *   settings.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The matched entity labels as a JSON response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Thrown if the selection settings key is not found in the key/value store
   *   or if it does not match the stored data.
   */
  public function handleAutocomplete(Request $request, $target_type, $selection_handler, $selection_settings_key) {
    $matches = [];
    // Get the typed string from the URL, if it exists.
    if ($input = $request->query->get('q')) {
      $typed_string = Tags::explode($input);
      $typed_string = mb_strtolower(array_pop($typed_string));

      // Selection settings are passed in as a hashed key of a serialized array
      // stored in the key/value store.
      $selection_settings = $this->keyValue->get($selection_settings_key, FALSE);
      if ($selection_settings !== FALSE) {
        $selection_settings_hash = Crypt::hmacBase64(serialize($selection_settings) . $target_type . $selection_handler, Settings::getHashSalt());
        if ($selection_settings_hash !== $selection_settings_key) {
          // Disallow access when the selection settings hash does not match the
          // passed-in key.
          throw new AccessDeniedHttpException('Invalid selection settings key.');
        }
      }
      else {
        // Disallow access when the selection settings key is not found in the
        // key/value store.
        throw new AccessDeniedHttpException();
      }

      $matches = $this->matcher->getMatches($target_type, $selection_handler, $selection_settings, $typed_string);
    }

    return new JsonResponse($matches);
  }



  /**
   * Handle the autocomplete request.
   *
   * @param Request $request
   * @paramstring $settings
   *  Serialized array of settings. Array elements:
   *  - target_type: The entity type id
   *  - bundles: array of bundles, empty for all bundles
   *  - field: Name of the field in the entity, empty for "label" key field
   *  - match_operator: CONTAINS or STARTS_WITH
   *  - match_count: No of items to return
   *  - tags: TRUE for tag stype search
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
//   public function autocompleteHandler(Request $request, $settings) {
//     $matches = [];
//     $input = $request->query->get('q');
//     if ($input) {
//       $typed_string = Tags::explode($input);
//       $typed_string = mb_strtolower(array_pop($typed_string));

//       $settings = unserialize($settings);
//       $target_type = $settings['target_type'];

//       $entity_def = \Drupal::entityTypeManager()->getDefinition($target_type);
//       $query = \Drupal::entityQuery($target_type);

//       if (!empty($settings['target_bundles'])) {
//         $bundles = $settings['target_bundles'];
//         $key = $entity_def->getKey('bundle');
//         if (count($bundles) == 1) {
//           $query->condition($key, reset($bundles));
//         }
//         else {
//           $or_group = $query->orConditionGroup();
//           foreach ($bundles as $bundle) {
//             $or_group->condition($key, $bundle);
//           }
//           $query->condition($or_group);
//         }
//       }

//       $field = empty($settings['field'])
//           ? $entity_def->getKey('label') : $settings['field'];

//       $query->condition($field, $typed_string, $settings['match_operator']);
//       $query->sort($field);
//       // And set the number of matches.
//       $query->range(0, $settings['match_count']);

//       $ids = $query->execute();
//       if ($ids) {
//         $entities = \Drupal::entityTypeManager()
//           ->getStorage($target_type)->loadMultiple($ids);
//         foreach ($entities as $entity) {
//           $text = $entity->get($field)->value;
//           $matches[] = [
//             'value' => $text,
//             'label' => $text,
//           ];
//         }
//       }
//     }
//     return new JsonResponse($matches);
//   }


}
